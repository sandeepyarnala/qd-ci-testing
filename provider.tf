terraform {
  required_providers {
    azurerm = {
      # ...
    }
  }
  backend "azurerm" {
    resource_group_name  = "testing-RG-1"
    storage_account_name = "sandy903603"
    container_name       = "terraform"
    key                  = "dev.terraform.tfstate"
  }
  required_version = ">= 0.13"
}

provider "azurerm" {
  features {}
}
