variable "rg_name" {
    description = "Name of the Resource_group"
    type = string
}

variable "rg_location" {
    description = "Location of the Resource remians same"
    type = string
}

variable "vnet_name" {
  description = "Name of virtual network"
  type        = string
}

variable "subnets" {
  description = "Name of the subnet name"
  type = list(any)
}

variable "network_interface_name" {
  description = "Name of network interface"
  type = string
}

variable "ip_addr_name" {
  description = "Name of ip address for NIC"
  type = string
  default = "internal"
}

variable "ip_addr_allocation" {
  description = "Name of ip address for NIC allocation"
  type = string
  default = "Dynamic"
}

variable "vmName" {
  description = "Name of VM's"
  type = list(any)
}

variable "vmSize" {
    type        = list(any)
    description = "(Optional) The SKU which should be used for this Virtual Machine."
}

variable "diskStorageAccountType" {
    type        = string
    description = "enter thre attached_disk_size value in GB"
}

variable "adminUsername" {
    type        = string
    description = "(Optional) The username of the local administrator used for the Virtual Machine. Changing this forces a new resource to be created." 
    default="beamer"
}

variable "adminPassword" {
    type        = string
    description = "(Optional)  The admin account password."
    default="Questdiag@123"
    sensitive = true
}

variable "osdisk_storagetype" {
    type        = string
    description = "(Optional) The Type of Storage Account which should back this the Internal OS Disk. Possible values are Standard_LRS, StandardSSD_LRS and Premium_LRS. Changing this forces a new resource to be created." 
}

variable "cachingType" {
    type        = string
    description = "Caching type for creation of VM"
}

variable "diskSize" {
    type        = string
    description = "OS disk size for VM creation"
}

variable "skuOS" {
    type        = string
    description = "OS SKU for VM creation"
}

# variable "diskName" {
#     type        = list(any)
#     description = "Disk_name to add it to existing_vm"
# }

variable "lun_num" {
    type        = list(any)
    description = "Disk_name to add it to existing_vm"
}

variable "manageDiskSize" {
    type        = number
    description = "Managed disk size to add it to existing_vm"
}

variable "manageDiskName" {
    type        = list(any)
    description = "Managed disk size to add it to existing_vm"
}

variable "creation_option" {
    type        = string
    description = "Creation option for managed disk"
    default = "Empty"
}

