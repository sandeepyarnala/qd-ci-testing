resource "azurerm_windows_virtual_machine" "vm" {
 name = var.vm_name
 resource_group_name = var.rg_name
 location = var.rg_location
 size = var.vm_size
 admin_username = var.admin_username
 admin_password = var.admin_password
 #source_image_id = var.image_id
 count = var.vm_count
 
 network_interface_ids = var.nic_id
 os_disk {
    name = "${var.vm_name}-OSDisk"
    caching = var.caching_type
    storage_account_type = var.os_disk_storage_type
    disk_size_gb = var.disk_size
 } 

 source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = var.os_sku
    version   = "latest"
  }
}




