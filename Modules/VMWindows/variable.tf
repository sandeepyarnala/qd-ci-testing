variable "vm_name" {
  description = "Name of the virtual machine"
  type        = string
}

variable "vm_count" {
  description = "Count of virtual machine"
  type        = number
  default = 1
}

variable "vm_size" {
  description = "Size of virtual machine"
  type        = string
}

variable "admin_username" {
  description = "Specify the user_name of the account."
  type        = string
  sensitive   = true
}

variable "admin_password" {
  description = "Specify the password associated to the account"
  type        = string
  sensitive   = true
}

variable "caching_type" {
  description = "Specifies the permissions of virtual machine"
  type        = string
}

variable "os_disk_storage_type" {
  description = "Specifies os disk type"
  type        = string
}

variable "disk_size" {
  description = "Specifies disk size of virtual machine"
  type        = string
}

variable "os_sku" {
  description = "Specifies the sku of image"
  type        = string
}

variable "nic_id" {
  description = "NIC ID of virtual machine"
  type        = list(string)
}
variable "rg_name" {
  description = "Resouce group name"
  type        = string
}

variable "rg_location" {
  description = "Location of Resouce group"
  type        = string
}

