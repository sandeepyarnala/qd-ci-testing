variable "subnet_name" {
  description = "Name of the subnet name"
  type = string
}

variable "vnet_name" {
  description = "Name of the vnet subnet_name"
  type = string
}

variable "rg_name" {
  description = "Name of the rg name"
  type = string
}