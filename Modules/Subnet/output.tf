 output "subnet_id_out"{
    value = data.azurerm_subnet.subnet.*.id
}

output "subnet_name_out"{
    value = data.azurerm_subnet.subnet.*.name
}