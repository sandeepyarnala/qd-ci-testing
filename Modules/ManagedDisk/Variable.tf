variable "diskname" {
  type = string
}

variable "rg_name" {
  type = string
}

variable "rg_location" {
  type = string
}

variable "disk_storage_account_type" {
  type = string
}

variable "creation_option" {
  type = string
}

variable "disk_size" {
  type = number
}

variable "lun" {
  type = string
}

variable "caching_type" {
  type = string
}

variable "vm_id" {
  type = string
}