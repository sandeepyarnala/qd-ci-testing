resource "azurerm_managed_disk" "data_disk" {
  name                 = var.diskname
  location             = var.rg_location
  resource_group_name  = var.rg_name
  storage_account_type = var.disk_storage_account_type
  create_option        = var.creation_option
  disk_size_gb         = var.disk_size
}
# Then we need to attach the data disk to the Azure virtual machine
resource "azurerm_virtual_machine_data_disk_attachment" "disk_attach" {
  managed_disk_id    = azurerm_managed_disk.data_disk.id
  virtual_machine_id = var.vm_id
  lun                = var.lun
  caching            = var.caching_type
}