variable "vnet_name" {
  description = "Name of virtual network"
  type        = string
}

variable "rg_name" {
  description = "Name of resource_group"
  type        = string
}