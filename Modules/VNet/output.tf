output "virtual_network_id_out"{
   value = data.azurerm_virtual_network.virtual_network.id
}

output "virtual_network_name_out"{
   value = data.azurerm_virtual_network.virtual_network.name
}