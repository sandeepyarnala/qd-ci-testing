##################### Resource_group starts here ######################################

module "Resource_group" {
    source = "./Modules/Resource_Group"
    rg_name = var.rg_name
}

##################### Resource_group ends here ######################################

##################### VNET starts here ######################################

module "VNet" {
  source = "./Modules/VNet"
  vnet_name = var.vnet_name
  rg_name = var.rg_name
}

##################### VNET ends here ######################################

##################### SubNet Starts here ######################################

module "Subnet" {
    source = "./Modules/Subnet"
    depends_on = [module.VNet]
    subnet_name = var.subnets[count.index]
    vnet_name = module.VNet.virtual_network_name_out
    rg_name = var.rg_name
    count = length(var.subnets)
}
##################### SubNet ends here ######################################

##################### Network_interface creation starts here ###########################

module "Network_interface" {
    source = "./Modules/Network_Interface"
    depends_on = [module.Subnet]
    count = length(var.vmName)
    nic_name = "${var.network_interface_name}-${count.index + 1}"
    rg_location = var.rg_location
    rg_name = var.rg_name
    ip_name = var.ip_addr_name
    subnet_id = "${element(module.Subnet.*.subnet_id_out[count.index], count.index)}"
    ip_allocation = var.ip_addr_allocation
}   

##################### Network_interface creation ends here #############################

#################### VM_Creation creation starts here ###########################

module "VM_creation" {
    source = "./Modules/VMWindows"
    vm_name = "${element (var.vmName, count.index)}"
    rg_name = var.rg_name
    rg_location = var.rg_location
    vm_size = "${element (var.vmSize, count.index)}"
    admin_username      = var.adminUsername
    admin_password      = var.adminPassword
    count = length(var.vmName)
    nic_id = ["${element(module.Network_interface.*.nic_id_out[count.index], count.index)}"]
    os_disk_storage_type = var.osdisk_storagetype
    caching_type         = var.cachingType
    disk_size =   var.diskSize
    os_sku = var.skuOS
}

#################### VM_Creation creation ends here ###########################

#################### Managed_disk creation starts here ###########################

module "managed_disk" {
  source = "./Modules/ManagedDisk"
  diskname = "${element (var.manageDiskName, count.index)}"
  rg_name = var.rg_name
  rg_location = var.rg_location
  disk_storage_account_type = var.diskStorageAccountType
  disk_size = var.manageDiskSize
  vm_id = "${element (module.VM_creation.*.vm_id_out[count.index], count.index)}"
  caching_type = var.cachingType
  creation_option = var.creation_option
  lun = "${element (var.lun_num, count.index)}"
  count = length(var.manageDiskName)
} 

#################### Managed_disk creation ends here ###########################
